// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        map:null,
        mapInfo:null,
        mapNum:null,
        mapWidth:null,
        map1:{
          default:null,
          type:cc.Prefab,
        },
        map2:{
          default:null,
          type:cc.Prefab,
        },
        changePosX:null,
        changePosY:null,
        currentX:null,
        currentY:null,
        //移動距離
        moveRange: 48,
        //移動にかかる時間
        moveDuration:0.5,
        //アクションが終了したら値が格納されるためアクション終了を認識するためのもの
        actionend: null,
        player:{
            default:null,
            type:cc.Prefab,
        },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
      let player = cc.instantiate(this.player);
      this.node.addChild(player, 3);
      this.map = cc.instantiate(this.map1);
      this.node.addChild(this.map,2);
      this.map.setPosition(cc.v2(-48,-48));
      this.mapNum = 1;
      this.mapWidth = 5;
      this.currentX = 2;
      this.currentY = 2;
      this.changePosX = [3,1];
      this.changePosY = [1,3];
      console.log("changePos" + this.changePosX);
      //this.changePos = [[3,3],[2,2]];
      this.mapInfo = [0,0,0,0,0,
                      0,1,1,2,0,
                      0,1,1,1,0,
                      0,2,1,1,0,
                      0,0,0,0,0];
      console.log(this.map);
      cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    },

    //---------------------------------------------------------
    //キーが押されたとき、eventの中にキーの情報なんかが入ってる
    //---------------------------------------------------------
    onKeyDown: function (event) {
      console.log("onKeyDown called");
      switch (event.keyCode) {
        case cc.macro.KEY.d:
          console.log("Press d");
          //移動先が存在する、移動可能か
          if (this.mapInfo[this.currentX + this.currentY * this.mapWidth + 1] === 0 || this.actionend !== null) {
            console.log("stop d");
            return;
          }else {
            console.log("move d");
            this.setLeftMove();
            this.currentX++;
          }
          break;

        case cc.macro.KEY.a:
          console.log("Press a");
          //移動先が存在する、移動可能か
          if (this.mapInfo[this.currentX + this.currentY * this.mapWidth - 1] === 0 || this.actionend !== null) {
            console.log("stop a");
            return;
          }else {
            this.setRightMove();
            this.currentX--;
            console.log("move a");
          }
          break;

        case cc.macro.KEY.s:
          console.log("Press s");
          //移動先が存在する、移動可能か
          if (this.mapInfo[this.currentX + (this.currentY + 1) * this.mapWidth] === 0 || this.actionend !== null) {
            console.log("stop s");
            return;
          }else {
            this.setUpMove();
            this.currentY++;
            console.log("move s");
          }
          break;

        case cc.macro.KEY.w:
          console.log("Press w");
          //移動先が存在する、移動可能か
          if (this.mapInfo[this.currentX + (this.currentY - 1) * this.mapWidth] === 0 || this.actionend !== null) {
            console.log("stop w");
            return;
          }else {
            this.setDownMove();
            this.currentY--;
            console.log("move w");
          }
          break;
         case cc.macro.KEY.q:
           console.log("X:" + this.currentX);
           console.log("Y:" + this.currentY);
           console.log(this.mapInfo);
           break;
        default:
          console.log("関係ないキー");
          break;
      }

      //切り替え地点チェック
      for (var i = 0; i < this.changePosX.length; i++) {
        if (this.changePosX[i] === this.currentX) {
          if (this.changePosY[i] === this.currentY) {
            console.log("Change MAP");
            this.mapChange(i);
          }
        }
      }
    },

    //--------------------------
    //移動の方向を指定する関数群
    //--------------------------
    setUpMove: function () {
      this.move(cc.v2(0,this.moveRange));
    },
    setDownMove: function () {
      this.move(cc.v2(0,-this.moveRange));
    },
    setRightMove: function () {
      this.move(cc.v2(this.moveRange,0));
    },
    setLeftMove: function () {
      this.move(cc.v2(-this.moveRange,0));
    },

    //--------------
    //移動を実行
    //--------------
    move: function (vector) {
      //moveBy(移動にかかる時間、移動距離)
      //cc.v2(x,y)移動するベクトル
      const move = cc.moveBy(this.moveDuration,vector);
      this.actionend = this.map.runAction(move);
      this.scheduleOnce(function() {
        this.actionend = null;
      }, this.moveDuration);
    },
    //-------------------------------
    //現在マップをもとにマップ切り替え
    //-------------------------------
    mapChange:function (changePosNum) {
      console.log("mapChange Called");
      console.log(this.mapNum);
      switch (this.mapNum) {
        case 1:
          this.map1SetPos(changePosNum);
          break;
        case 2:
          this.map2SetPos(changePosNum);
          break;
        default:
          break;
      }
    },

    //-----------------------------------
    //切り替え地点をもとにマップを切り替え
    //-----------------------------------
    //map1分
    map1SetPos:function (changePosNum) {
        console.log("map1SetPosCalled ChangePos=" + changePosNum);
      switch (changePosNum) {
        case 0:
          this.currentX = 2;
          this.currentY = 1;
          this.map2Init();
          this.map.setPosition(cc.v2(48 * (this.currentX - 1),48 * (this.currentY - 2)));
          break;
        case 1:
          this.currentX = 1;
          this.currentY = 2;
          this.map2Init();
          this.map.setPosition(cc.v2(48 * (this.currentX - 1),48 * (this.currentY - 2)));
          break;
        default:
          break;
      }
    },

    map2SetPos:function (changePosNum){
        console.log("map2SetPosCalled ChangePos=" + changePosNum);
        switch (changePosNum) {
            case 0:
                this.currentX = 1;
                this.currentY = 3;
                this.map1Init();
                console.log("nandeya");
                this.map.setPosition(cc.v2(48 * (this.currentX - 1),48 * (this.currentY - 2)));
                break;
            case 1:
                this.currentX = 3;
                this.currentY = 1;
                this.map1Init();
                this.map.setPosition(cc.v2(48 * (this.currentX - 1),48 * (this.currentY - 2)));
                break;
        }
    },
    //----------------------------------
    //マップ初期化群
    //----------------------------------
    map1Init:function(){
        this.node.children[1].destroy();
        this.map = cc.instantiate(this.map1);
        this.node.addChild(this.map,2);
        this.mapNum = 1;
        this.mapWidth = 5;
        this.changePosX = [3,1];
        this.changePosY = [1,3];
        this.mapInfo = [0,0,0,0,0,
            0,1,1,2,0,
            0,1,1,1,0,
            0,2,1,1,0,
            0,0,0,0,0];
    },

    map2Init:function () {
      //力技で前のマップを削除
      console.log(this.node.children[2]);
      this.node.children[1].destroy();
      //this.node.getChildByName(map1).destroy();
      this.map = cc.instantiate(this.map2);
      this.node.addChild(this.map);
      this.mapNum = 2;
      this.mapWidth = 4;
      console.log("map2instans");
      this.changePosX = [1,2];
      this.changePosY = [2,1];
      this.mapInfo = [0,0,0,0,
                      0,1,2,0,
                      0,2,1,0,
                      0,0,0,0,];
    }
    //start () {},

    // update (dt) {},
});
